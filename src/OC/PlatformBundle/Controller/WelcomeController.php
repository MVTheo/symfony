<?php

namespace OC\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class WelcomeController extends Controller
{
    public function indexAction()
    {
        /* return $this->render('OCPlatformBundle:Default:index.html.twig'); */
        return $this->render('@OCPlatform/pages/welcome.html.twig');
    }
}
