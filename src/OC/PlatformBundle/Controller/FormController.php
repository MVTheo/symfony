<?php

namespace OC\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FormController extends Controller
{
    public function indexAction()
    {
        /* return $this->render('OCPlatformBundle:Default:index.html.twig'); */
        return $this->render('@OCPlatform/pages/form.html.twig');
    }
}
